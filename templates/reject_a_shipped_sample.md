# README #

Reject a shipped sample (may also need to delete shipment since it is the only sample in the shipment).

## Template parameters ##

- $SHIPMENT_ID - The shipment to reject.
- $CASE_ID - The case.
- $ORDER_ID - The order to update.
