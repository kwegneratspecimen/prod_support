-- $SHIPMENT_ID - The shipment to reject.
-- $CASE_ID - The case.
-- $ORDER_ID - The order to update.

update shipment_requests set status = 'cancelled' where id = $SHIPMENT_ID
delete from ship_pick_list_items where ship_req_id = $SHIPMENT_ID

select * from pick_list_items where supplier_case_id = '$CASE_ID' and order_item_id = $ORDER_ID

update pick_list_items set status = 'aliquot_done' -- shipped
where status = 'shipped' and supplier_case_id = '$CASE_ID' and order_item_id = $ORDER_ID
