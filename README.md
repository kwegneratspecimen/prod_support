# README #

This repository stores scripts and procedures related to Product Support.

### What is this repository for? ###

* As new Product Support tickets come in, first check here to find out whether any script templates exist here to solve the Product Support issue you are handling.
* If no script templates exist, then you must create new scripts to handle this issue, and store them here.

### Contributing new Product Support scripts ###

- Clone the *prod_support* repo.
- Make a copy of the *prod_support/_template* template files into the *templates* directory. 
- Each support operation must consist of 2 scripts and a README:
	- An execution script, to accomplish the task, with the suffix **_do.sql**.
	- A rollback script, to undo the task, with the suffix **_undo.sql**.
	- A README file, with the the suffix **.md**.
- Both scripts should have the same name prefix.
- Script names must consist only of lower-case, alphanumeric characters, with underscores to delimit words.
- Script names should be descriptive of what they do. For example, for a script to delete a user, there would be two scripts and a README:
	- delete_user_do.sql
	- delete_user_undo.sql
	- delete_user.md
- Follow the format and structure of existing templates.
- Commit your changes to the *prod_support* repo and issue a pull request.

### Handling product support tickets ###

- Branch and clone the *prod_support* repo where the branch name should be the JIRA ticket name.
- Once you have selected the template you need to use, or have contributed your own scripts, now make a sub-folder in the *tickets* subdirectory.
- The sub-folder should be given the same name as the JIRA ticket.
- Copy the SQL templates you will use into the sub-directory, and edit them there, along with the README.
- Follow the format and structure of existing tickets.
- In JIRA, for the Product Support ticket, indicate what template scripts were used.
- Commit your changes to the *prod_support* repo and issue a pull request.
- For now, just merge your branch into *master*.
