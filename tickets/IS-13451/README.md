# README #

Reject a shipped sample (may also need to delete shipment since it is the only sample in the shipment).

## Scenario ##

We recently had a sample accessioned and shipped for project 11040 that did not meet the minimum volume requirement for the project. Is it possible to have this case and/or shipment rejected in MP? The Case ID is 110400013, Shipment ID is 458-5513.

## Running ##

Run the provided scripts.
